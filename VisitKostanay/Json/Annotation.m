//
//  Annotation.m
//  Master of
//
//  Created by Almas on 08.10.15.
//  Copyright © 2015 Almas. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

- (void)dealloc {
    self.title = nil;
    self.subtitle = nil;
    
}



@end