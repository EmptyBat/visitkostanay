//
//  Location.m
//  JSONHandler
//
//  Created by Phillipus on 28/10/2013.
//  Copyright (c) 2013 Dada Beatnik. All rights reserved.
//

#import "Location.h"

@implementation Location

// Init the object with information from a dictionary
- (id)initWithJSONDictionary:(NSDictionary *)jsonDictionary {
    if(self = [self init]) {
  
        // Assign all properties with keyed values from the dictionary
        _title = [jsonDictionary objectForKey:@"title_kz"];
        _iid = [jsonDictionary objectForKey:@"ID"];
        _content_kz = [jsonDictionary objectForKey:@"content_kz"];
        _content = [jsonDictionary objectForKey:@"content"];
        _subject = [jsonDictionary objectForKey:@"subject"];
        _address = [jsonDictionary objectForKey:@"address"];
        _coordinates = [jsonDictionary objectForKey:@"coordinates"];
        _rdate = [jsonDictionary objectForKey:@"rdate"];
        _surname = [jsonDictionary objectForKey:@"surname"];
        _name = [jsonDictionary objectForKey:@"name"];
           _picture = [jsonDictionary objectForKey:@"picture"];
        _name_kz = [jsonDictionary objectForKey:@"name_kz"];
        _fio_kz = [jsonDictionary objectForKey:@"fio_kz"];
        _contacts_kz = [jsonDictionary objectForKey:@"contacts_kz"];
        _address_kz = [jsonDictionary objectForKey:@"address_kz"];
        _lat = [jsonDictionary objectForKey:@"lat"];
        _lang = [jsonDictionary objectForKey:@"lng"];
        _contacts = [jsonDictionary objectForKey:@"contacts"];
        
        if ([jsonDictionary objectForKey:@"title_kz"] == [NSNull null]) { _title=nil;}
        if ([jsonDictionary objectForKey:@"ID"] == [NSNull null]) { _iid=nil;}
        if ([jsonDictionary objectForKey:@"content_kz"] == [NSNull null]) { _content_kz=nil;}
        if ([jsonDictionary objectForKey:@"content"] == [NSNull null]) { _content=nil;}
        if ([jsonDictionary objectForKey:@"subject"] == [NSNull null]) { _subject=nil;}
        if ([jsonDictionary objectForKey:@"address"] == [NSNull null]) { _address=nil;}
        if ([jsonDictionary objectForKey:@"coordinates"] == [NSNull null]) { _coordinates=nil;}
        if ([jsonDictionary objectForKey:@"rdate"] == [NSNull null]) { _rdate=nil;}
        if ([jsonDictionary objectForKey:@"surname"] == [NSNull null]) { _surname=nil;}
        if ([jsonDictionary objectForKey:@"name"] == [NSNull null]) { _name=nil;}
        if ([jsonDictionary objectForKey:@"picture"] == [NSNull null]) { _picture=nil;}
        if ([jsonDictionary objectForKey:@"name_kz"] == [NSNull null]) { _name_kz=nil;}
        if ([jsonDictionary objectForKey:@"fio_kz"] == [NSNull null]) { _fio_kz=nil;}
        if ([jsonDictionary objectForKey:@"contacts_kz"] == [NSNull null]) { _contacts_kz=nil;}
        if ([jsonDictionary objectForKey:@"address_kz"] == [NSNull null]) { _address_kz=nil;}
        if ([jsonDictionary objectForKey:@"lat"] == [NSNull null]) { _lat=nil;}
        if ([jsonDictionary objectForKey:@"lng"] == [NSNull null]) { _lang=nil;}
                if ([jsonDictionary objectForKey:@"contacts"] == [NSNull null]) { _contacts=nil;}
    }
    
    return self;
    }

@end
