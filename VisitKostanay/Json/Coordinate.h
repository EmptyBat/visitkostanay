//
//  Coordinate.h
//  Shymkent.GOV
//
//  Created by Almas on 18.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Coordinate : NSObject
// Return an array of Location objects from the json file at location given by url
- (NSArray *)locationsFromJSONFile:(NSURL *)url;
@end
