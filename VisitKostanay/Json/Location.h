//
//  Location.h
//  JSONHandler
//
//  Created by Phillipus on 28/10/2013.
//  Copyright (c) 2013 Dada Beatnik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject

- (id)initWithJSONDictionary:(NSDictionary *)jsonDictionary;

@property (readonly) NSString *title;
@property (readonly) NSString *iid;
@property (readonly) NSString *content_kz;
@property (readonly) NSString *subject;
@property (readonly) NSString *content;
@property (readonly) NSString *address;
@property (readonly) NSString *coordinates;
@property (readonly) NSString *rdate;
@property (readonly) NSString *surname;
@property (readonly) NSString *name;
@property (readonly) NSString *picture;
@property (readonly) NSString *name_kz;
@property (readonly) NSString *fio_kz;
@property (readonly) NSString *contacts_kz;
@property (readonly) NSString *address_kz;
@property (readonly) NSString *lat;
@property (readonly) NSString *lang;
@property (readonly) NSString *contacts;
@end
