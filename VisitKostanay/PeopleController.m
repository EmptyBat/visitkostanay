//
//  PeopleController.m
//  Shymkent.GOV
//
//  Created by Almas on 05.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import "PeopleController.h"
#import "Location.h"
#import "SVProgressHUD.h"
#import "Help_detailis.h"
#import "Reachability.h"
#import "People_location.h"
#import "SWRevealViewController.h"
@interface PeopleController ()
{NSArray*one;}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_btn;
@end

@implementation PeopleController

- (void)viewDidLoad {
    [super viewDidLoad];
    _menu_btn.target = self.revealViewController;
    _menu_btn.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:@"Нет интернет соединения!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alrt show];
        
    } else {
        [SVProgressHUD show];
        People_location *jsonLoader = [[People_location alloc] init];
        NSURL *blogURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://shymkent.gov.kz/kz/Api?content=halyk"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            one = [jsonLoader locationsFromJSONFile:blogURL];
            NSLog(@"123%@",one);
            [self.TableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            [SVProgressHUD dismiss];
        });

    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return one.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"1";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    Location*location=[one objectAtIndex:indexPath.row];
    UILabel*number=(UILabel*)[cell viewWithTag:1];
    UILabel*subject=(UILabel*)[cell viewWithTag:2];
     UILabel*adress=(UILabel*)[cell viewWithTag:3];
     UILabel*surname=(UILabel*)[cell viewWithTag:4];
     UILabel*date=(UILabel*)[cell viewWithTag:5];
    number.text=[NSString stringWithFormat:@"№%@",location.iid];
    subject.text=location.subject;
    adress.text=location.address;
    subject.text=location.subject;
    surname.text=location.surname;
    date.text=location.rdate;
    return cell;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Detail"]){
    Help_detailis*help=segue.destinationViewController;
      NSIndexPath*indexPath = [self.TableView indexPathForCell:sender];
    Location*location=[one objectAtIndex:indexPath.row];
    help.date=location.rdate;
    help.surname=location.surname;
    help.name=location.name;
    help.address=location.address;
    help.subject=location.subject;
    help.content=location.content;
    help.title=location.subject;
    }
    else {}
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)list:(id)sender {
       UIActionSheet *sorp = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles: @"ЕГЕР МЕН ӘКІМ БОЛСАМ", @"ӨТІНІШТІ ТОЛТЫРУ", nil]; sorp.tag = 1; [sorp showInView:[UIApplication sharedApplication].keyWindow];
 }
- ( void ) actionSheet : ( UIActionSheet * ) actionSheet clickedButtonAtIndex : ( NSInteger ) buttonIndex {
        if (buttonIndex==0){
 
                    [self performSegueWithIdentifier:@"akim" sender:self];
            
        }
        else if (buttonIndex==1){
            NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                [self performSegueWithIdentifier:@"thanks" sender:self];

        }
    
    NSLog ( @ "Index = %ld - Title = %@" , (long)buttonIndex, [ actionSheet buttonTitleAtIndex : buttonIndex ] ) ;
}
@end
