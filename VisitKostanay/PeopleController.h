//
//  PeopleController.h
//  Shymkent.GOV
//
//  Created by Almas on 05.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeopleController : UIViewController<UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UITableView *TableView;
- (IBAction)list:(id)sender;

@end
