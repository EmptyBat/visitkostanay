//
//  FeedBack.m
//  Shymkent.GOV
//
//  Created by Almas on 05.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import "FeedBack.h"
#import "SWRevealViewController.h"
@interface FeedBack ()
{NSArray*count;}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_btn;

@end
@implementation FeedBack
- (void)viewDidLoad {
    [super viewDidLoad];
    _menu_btn.target = self.revealViewController;
    _menu_btn.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    count=@[@"1",@"2",@"3"];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return count.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0){
        static NSString *simpleTableIdentifier = @"1";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        return cell;
    }
    else if (indexPath.row==1){
        static NSString *simpleTableIdentifier = @"2";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        return cell;
    
    }
    else {
        static NSString *simpleTableIdentifier = @"3";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        return cell;
   }
 
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{   if (indexPath.row==0){
    return 180.0;
}
    else if (indexPath.row==1){
    return 85.0;
}
    else {
    return 75.0;

}
    }
@end
