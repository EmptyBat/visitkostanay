//
//  Help_messages.m
//  Shymkent.GOV
//
//  Created by Almas on 05.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import "Help_messages.h"
#import "SVProgressHUD.h"
@implementation Help_messages
- (void)viewDidLoad {
    [super viewDidLoad];
    self.Text.maxHeight=400;
    _surname.delegate=self;
    _name.delegate=self;
    _fname.delegate=self;
    _email.delegate=self;
    _Text.delegate=self;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];

}
- (void) hideKeyboard {
    [_surname resignFirstResponder];
    [_name resignFirstResponder];
    [_fname resignFirstResponder];
    [_email resignFirstResponder];
    [_Text resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.surname){
        [self.name becomeFirstResponder];
    }
    else if (theTextField==self.name){
        [self.fname becomeFirstResponder];
        
    }
    else if (theTextField==self.fname){
        [self.email becomeFirstResponder];
        
    }
    else if (theTextField==self.email){
        [self.Text becomeFirstResponder];
        
    }
    return YES;
}
- (IBAction)send_message:(id)sender {

        if (![_surname.text isEqualToString:@""]&&![_name.text isEqualToString:@""]&&![_fname.text isEqualToString:@""]&&![self.Text.text isEqualToString:@""]&&![_email.text isEqualToString:@""]){
            [SVProgressHUD show];
            NSString *params = [NSString stringWithFormat:@"&surname=%@&name=%@&&fname=%@&&email=%@&&content==%@",_surname.text,_name.text,_fname.text,_email.text,_Text.text];
            NSURL* url = [NSURL URLWithString:@"http://shymkent.gov.kz/kz/Api?akim"];             NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];             NSURLResponse *responce;
            [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
            NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
            NSLog(@"%@",theReply);
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:@"Ваше сообщение отправлено" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alrt show];
                 [self hideKeyboard];
            self.surname.text=@"";
            self.name.text=@"";
            self.fname.text=@"";
            self.email.text=@"";
            self.Text.text=@"";
            }
        else {
            UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:@"Заполните все поля!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alrt show];
        }

}
@end
