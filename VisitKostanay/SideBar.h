//
//  SideBar.h
//  Get Me
//
//  Created by Bugin Group on 06.03.15.
//  Copyright (c) 2015 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>
NSInteger inter;
@interface SideBar :UITableViewController;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

@end
