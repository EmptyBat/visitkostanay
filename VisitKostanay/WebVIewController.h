//
//  WebVIewController.h
//  Shymkent.GOV
//
//  Created by Almas on 05.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebVIewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *WebVIew;
@property(nonatomic)NSString*html;
@end
