//
//  Webview_Detail.m
//  Shymkent.GOV
//
//  Created by Almas on 20.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import "Webview_Detail.h"
#import "JSONLoader.h"
#import "Location.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
@interface Webview_Detail ()
{
    NSArray*count;
}
@end

@implementation Webview_Detail

- (void)viewDidLoad {
    [super viewDidLoad];
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:@"Нет интернет соединения!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alrt show];
    }
    else {
        [SVProgressHUD show];
        JSONLoader *jsonLoader = [[JSONLoader alloc] init];
        NSURL *blogURL ;
        if (_check_url==1){
             blogURL = [NSURL URLWithString:[_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:blogURL];
            [_WebView loadRequest:urlRequest];
            NSLog(@"%@",_url);
        }
        else {

         blogURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://shymkent.gov.kz/kz/Api?page=%@&lang=kz",_url]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            count = [jsonLoader locationsFromJSONFile:blogURL];
            Location*location=[count objectAtIndex:0];
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenWidth = screenRect.size.width;
            NSString *setImageRule = [NSString stringWithFormat:@"addCSSRule('img alt', max-width:%f; width:auto; height: auto;')", screenWidth];
            
            [_WebView stringByEvaluatingJavaScriptFromString:setImageRule];
            NSString*content=[NSString stringWithFormat:@"<body>%@</body>",location.content_kz];
            [_WebView loadHTMLString:[content stringByReplacingOccurrencesOfString:@"\n" withString:@""] baseURL:nil];
            [SVProgressHUD dismiss];
            
        });
        }
    }
}
-(BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;
    NSLog(@"123,%@",urlString);
    if (![urlString isEqualToString:@"about:blank"]){
        [SVProgressHUD show];
        NSLog(@"123,%@",urlString);
        NSString *numberString;
        NSScanner *scanner = [NSScanner scannerWithString:urlString];
        NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        // Throw away characters before the first number.
        [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
        
        // Collect numbers.
        [scanner scanCharactersFromSet:numbers intoString:&numberString];
        
        
        JSONLoader *jsonLoader = [[JSONLoader alloc] init];
        NSURL *blogURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://shymkent.gov.kz/kz/Api?page=%@&lang=kz",numberString]];
        dispatch_async(dispatch_get_main_queue(), ^{
            count = [jsonLoader locationsFromJSONFile:blogURL];
            Location*location=[count objectAtIndex:0];
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenWidth = screenRect.size.width;
            NSString *setImageRule = [NSString stringWithFormat:@"addCSSRule('img alt', max-width:%f; width:auto; height: auto;')", screenWidth];
            [_WebView stringByEvaluatingJavaScriptFromString:setImageRule];
            NSString*content=[NSString stringWithFormat:@"<body>%@</body>",location.content_kz];
            [_WebView loadHTMLString:[content stringByReplacingOccurrencesOfString:@"\n" withString:@""] baseURL:nil];
            [SVProgressHUD dismiss];
            
        });
        return YES;
    }
    else {
        if (_check_url==1){
            return NO;
        }
        else {
                return YES;
        }
      
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.WebView = nil;
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}


@end
