//
//  Webview_Detail.h
//  Shymkent.GOV
//
//  Created by Almas on 20.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Webview_Detail : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *WebView;
@property(nonatomic)NSString*url;
@property(nonatomic)int check_url;
@end
