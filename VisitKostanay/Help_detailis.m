//
//  Help_detailis.m
//  Shymkent.GOV
//
//  Created by Almas on 05.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import "Help_detailis.h"

@interface Help_detailis ()

@end

@implementation Help_detailis

- (void)viewDidLoad {
    [super viewDidLoad];
    self.TableView.estimatedRowHeight = 211.0;
    self.TableView.rowHeight = UITableViewAutomaticDimension;
    [self.navigationItem setHidesBackButton:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0){
    static NSString *simpleTableIdentifier = @"1";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel*text=(UILabel*)[cell viewWithTag:1];
        text.text=_content;
    return cell;
    }
    else {
        static NSString *simpleTableIdentifier = @"2";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        UILabel*text=(UILabel*)[cell viewWithTag:2];
        text.text=[NSString stringWithFormat:@"%@\n%@ %@\n%@",_address,_surname,_name,_date];
        return cell;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)Back:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
