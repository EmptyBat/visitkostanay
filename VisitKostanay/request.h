//
//  request.h
//  Shymkent.GOV
//
//  Created by Almas on 20.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AUIAutoGrowingTextView.h"
@interface request : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *subject;
@property (weak, nonatomic) IBOutlet UIButton *Maps_check;
- (IBAction)maps_check:(id)sender;
@property (weak, nonatomic) IBOutlet AUIAutoGrowingTextView *content;
- (IBAction)send_message:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *adress;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *Exit;
- (IBAction)exit:(id)sender;

@end
