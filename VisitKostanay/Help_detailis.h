//
//  Help_detailis.h
//  Shymkent.GOV
//
//  Created by Almas on 05.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Help_detailis : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property(nonatomic)NSString*date;
@property(nonatomic)NSString*surname;
@property(nonatomic)NSString*name;
@property(nonatomic)NSString*content;
@property(nonatomic)NSString*subject;
@property(nonatomic)NSString*address;
@end
