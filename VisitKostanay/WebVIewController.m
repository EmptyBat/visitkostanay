//
//  WebVIewController.m
//  Shymkent.GOV
//
//  Created by Almas on 05.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import "WebVIewController.h"

@implementation WebVIewController
- (void)viewDidLoad {
    [super viewDidLoad];


    NSString*content=[NSString stringWithFormat:@"<html><head><style>img{max-width:100%%;height:auto !important;width:auto !important;};</style></head><body style='margin:0; padding:0;'>%@</body></html>",_html];
    [_WebVIew loadHTMLString:[content stringByReplacingOccurrencesOfString:@"\n" withString:@""] baseURL:nil];

}
- (void)viewDidUnload
{
    [super viewDidUnload];
    self.WebVIew = nil;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    NSString *setImageRule = [NSString stringWithFormat:@"addCSSRule(img{"
                              "position: absolute;"
                              "top: 0;"
                              "bottom: 0;"
                              "left: 0;"
                              "right: 0;"
                              "margin: auto;"
                              "max-width: 100%;"
                              "max-height: 100%;"
                              "}" ];
    
    [_WebVIew stringByEvaluatingJavaScriptFromString:setImageRule];
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
    // If you are not using ARC:
    // return [[UIView new] autorelease];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}
@end
