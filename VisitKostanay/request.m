//
//  request.m
//  Shymkent.GOV
//
//  Created by Almas on 20.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import "request.h"
#import "SVProgressHUD.h"
@interface request ()

@end

@implementation request

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:nil forKey:@"coordinates"];
    self.content.maxHeight=400;
    _adress.delegate=self;
    _subject.delegate=self;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.subject){
        [self.adress becomeFirstResponder];
    }
    else if (theTextField==self.adress){
       [self.content becomeFirstResponder];
    }
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if([user objectForKey:@"coordinates"] !=nil ){
        [_Maps_check setTitle:@"Картадан көрсетілген" forState:UIControlStateNormal];
        
    }
    
}

- (void) hideKeyboard {
    [_subject resignFirstResponder];
    [_content resignFirstResponder];
    [_adress resignFirstResponder];
}
- (IBAction)maps_check:(id)sender {
    
}
- (IBAction)send_message:(id)sender {

    NSString *login_id = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"login_id"];
    NSString *coordinates = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"coordinates"];
    if (![_subject.text isEqualToString:@""]&&![_content.text isEqualToString:@""]&&![_adress.text isEqualToString:@""]){
            [SVProgressHUD show];
        NSString *params = [NSString stringWithFormat:@"&subject=%@&&content=%@&&address=%@&&coordinates=%@,",_subject.text,_content.text,login_id,coordinates];
        NSURL* url = [NSURL URLWithString:@"http://shymkent.gov.kz/kz/Api?otinish"];
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
        NSLog(@"%@",params);
        request.HTTPMethod = @"POST";
        request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];             NSURLResponse *responce;
        [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
        NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
        NSLog(@"%@",theReply);
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:@"Ваше сообщение отправлено" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alrt show];
        _content.text=@"";
        _subject.text=@"";
        _adress.text=@"";
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        [user setObject:nil forKey:@"coordinates"];
        [self hideKeyboard];
    }
    else {
        UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:@"Заполните все поля!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alrt show];
    }

}
- (IBAction)exit:(id)sender {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:nil forKey:@"login_id"];
      [user setObject:nil forKey:@"coordinates"];
[self.navigationController popToRootViewControllerAnimated:YES];
}
@end
