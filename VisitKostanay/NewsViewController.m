//
//  NewsViewController.m
//  Shymkent.GOV
//
//  Created by Almas on 05.01.16.
//  Copyright © 2016 None. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsLoader.h"
#import "Location.h"
#import "SVProgressHUD.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "WebVIewController.h"
#import "Reachability.h"
#import "SWRevealViewController.h"
@interface NewsViewController ()
{
    NSArray*count;
 
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_btn;
@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _menu_btn.target = self.revealViewController;
    _menu_btn.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        _activity_main.hidden=true;
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:@"Нет интернет соединения!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alrt show];
  }
    else {
        [SVProgressHUD show];
        NewsLoader *jsonLoader = [[NewsLoader alloc] init];
        NSURL *blogURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://shymkent.gov.kz/kz/Api?api_news=0&lang=kz"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            count = [jsonLoader locationsFromJSONFile:blogURL];
            [self.TableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            [SVProgressHUD dismiss];
        });
  
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return count.count;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == count.count-1) {
        num=num+10;
        [self update];
    }
}
-(void)update{
    _activity_main.hidden=false;
    _act.hidden=false;
    [_act startAnimating];
    _act.hidesWhenStopped = YES;
    NewsLoader *jsonLoader = [[NewsLoader alloc] init];
    NSURL *url ;
    
    NSString*str=[NSString stringWithFormat:@"%i",num];
    NSString*ur=[NSString stringWithFormat:@"http://shymkent.gov.kz/kz/Api?api_news=%@&lang=kz",str];
    
    url=   [NSURL URLWithString:[ur stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    // Load the data on a background queue...
    // As we are using a local file it's not really necessary, but if we were connecting to an online URL then we'd need it
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSArray*   bad = [jsonLoader locationsFromJSONFile:url];
        // Now that we have the data, reload the table data on the main UI thread
        
        count=[count arrayByAddingObjectsFromArray:bad];
        [self.TableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
        dispatch_queue_t queue = dispatch_get_global_queue(
                                                           DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSStringEncoding encoding;
            //Load the json on another thread
            NSString *jsonreturn = [[NSString alloc] initWithContentsOfURL:url usedEncoding:&encoding error:NULL];
            
            //When json is loaded stop the indicator
            [_act performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
        });
    }
                   );
    
    
};
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"1";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    Location *location = [count objectAtIndex:indexPath.row];
    UILabel*text=(UILabel*)[cell viewWithTag:3];
        UILabel*date=(UILabel*)[cell viewWithTag:8];
    text.text=location.title;
    date.text=location.rdate;
    NSString*obj=[NSString stringWithFormat:@"http://shymkent.gov.kz/assets/images/news/%@",location.picture];
    NSLog(@"133,%@",obj);
    NSURL *imageURL = [NSURL URLWithString:obj];
    NSString *key = [obj MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        UIImageView *imgView = (UIImageView*)[cell viewWithTag:1];
        imgView.image = image;
    
    } else {
        UIImageView *imgView = (UIImageView*)[cell viewWithTag:1];
        imgView.image = [UIImage imageNamed:@"icon.png"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            [FTWCache setObject:data forKey:key];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_sync(dispatch_get_main_queue(), ^{
                imgView.image = image;
             

                
            });
        });
    }

    return cell;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath*indexPath = [self.TableView indexPathForCell:sender];
    Location*location=[count objectAtIndex:indexPath.row];
    WebVIewController*controller=segue.destinationViewController;
    controller.html=location.content_kz;
    controller.title=location.title;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
