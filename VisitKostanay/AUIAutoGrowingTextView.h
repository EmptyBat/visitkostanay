//
//  Polzovatel.m
//  Master of
//
//  Created by Bugin Group on 18.02.15.
//  Copyright (c) 2015 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AUIAutoGrowingTextView : UITextView

@property (nonatomic) CGFloat maxHeight;
@property (nonatomic) CGFloat minHeight;

// TODO:
//@property(nonatomic) UIControlContentVerticalAlignment verticalAlignment;

@end


